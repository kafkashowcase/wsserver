## Description

Middleware between kafka and exchange-demo.
Listens on topic exchanges.
Provides data via websocket at localhost:3210

## Installation

### env properties

1. Create in root folder of this project file:

```shell
touch .env
```

2. Set following properties:

```shell
KAFKA_BROKER=localhost:9092
```

### libraries
```bash
$ yarn install
```

## Running the app

```bash
# watch mode
$ yarn start:dev
```

## Useful statements to work with kafka/ksql

### CREATE exchanges TOPIC
```shell
bin/kafka-topics.sh --zookeeper localhost:2181 --create --partitions 1 --replication-factor 1 --topic exchanges
```

### GENERATE EXCHANGES TEST DATA WITH ksql-datagen:
```shell
ksql-datagen schema=testdata/exchanges.avro format=json bootstrap-server=localhost:9092 topic=exchanges msgRate=1 iterations=100 key=value nThreads=1
```
