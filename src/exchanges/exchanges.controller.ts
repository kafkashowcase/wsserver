import {Controller, OnModuleInit} from '@nestjs/common';
import {Client, ClientKafka, EventPattern} from "@nestjs/microservices";
import {kafkaExchangesConfig} from "../settings/kafkaExchangesConfig";
import {WebSocketServer} from "@nestjs/websockets";
import {Server, WebSocketClient} from "ws";

@Controller('exchanges')
export class ExchangesController implements OnModuleInit{

    @Client(kafkaExchangesConfig)
    client: ClientKafka;

    @WebSocketServer()
    server: Server;

    async onModuleInit(): Promise<void> {
        console.log("on module init start");
        this.client.subscribeToResponseOf("exchanges");
        this.server = new Server({port: 3210});
        console.log("on module init stop");
    }

    @EventPattern("exchanges")
    async handleSignup(payload: any) {
        // NOTE: print result in terminal
        console.log(payload.value.value);
        this.server.clients.forEach(
            (client: WebSocketClient): void => {
                client.send(payload.value.value); // NOTE: send values to websocket clients
            }
        )
    }
}
