import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {kafkaExchangesConfig} from "./settings/kafkaExchangesConfig";
import {WsAdapter} from "@nestjs/platform-ws";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice(kafkaExchangesConfig);
  await app.startAllMicroservicesAsync();
  app.useWebSocketAdapter(new WsAdapter(app));
  await app.listen(3000);
}
bootstrap();
