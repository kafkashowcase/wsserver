import {KafkaOptions, Transport} from "@nestjs/microservices";

export const kafkaExchangesConfig: KafkaOptions = {
    transport: Transport.KAFKA,
    options: {
        client: {
            brokers: [process.env.KAFKA_BROKER],
        },
        consumer: {
            groupId: "nestExchange",
        }
    }
}
